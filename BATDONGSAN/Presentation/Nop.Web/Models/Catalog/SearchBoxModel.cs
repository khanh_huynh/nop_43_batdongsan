﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Directory;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public partial class SearchBoxModel : BaseNopModel
    {
        public bool AutoCompleteEnabled { get; set; }
        public bool ShowProductImagesInSearchAutoComplete { get; set; }
        public int SearchTermMinimumLength { get; set; }
        public bool ShowSearchBox { get; set; }

        public List<StateProvince> StateProvinces { get; set; }
        public List<SelectListItem> Prices { get; set; }
        public List<SelectListItem> Areas { get; set; }
    }
}