﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.News
{
    /// <summary>
    /// Represents a news item model
    /// </summary>
    public partial class NewsItemModelApi
    {
        
        
 
        public string Title { get; set; }

        public string Short { get; set; }

        public string Full { get; set; }

        public string MetaKey { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }

    }
}