namespace Nop.Web.Areas.Admin.Models.Api
{
    public class ApiJsonResult
    {
        public bool Success { get; set; }
        public object Data { get; set; }
    }
}