using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.News;
using Nop.Services.Catalog;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.News;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.News;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class NewsApiController : BaseAdminAnonymousController
    {
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly INewsModelFactory _newsModelFactory;
        private readonly INewsService _newsService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IStoreService _storeService;
        private readonly IUrlRecordService _urlRecordService;

        private string _privateKey = "NP430_BDS";
        private IProductService _productService;
        public NewsApiController(ICustomerActivityService customerActivityService,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            INewsModelFactory newsModelFactory,
            INewsService newsService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IProductService productService,
            IUrlRecordService urlRecordService)
        {
            _customerActivityService = customerActivityService;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _newsModelFactory = newsModelFactory;
            _newsService = newsService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _storeMappingService = storeMappingService;
            _storeService = storeService;
            _urlRecordService = urlRecordService;
            _productService = productService;
        }


        [HttpPost]
        public virtual JsonResult  Add([FromBody] NewsItemModelApi model, [FromHeader] string authorization)
        {
            if(string.IsNullOrEmpty(authorization)) return new JsonResult("Unauthorize");
            var hash = _privateKey + DateTime.Now.ToString("yyyyMMdd");
                if(!VerifyMd5Hash(hash,authorization)) return new JsonResult("Unauthorize");

            if (ModelState.IsValid)
            {
               
                var exxits = _newsService.GetNewsByMetaKey(model.MetaKey);
                if(exxits != null)    
                    return new JsonResult("EXIST");
                var modelEntity = new NewsItemModel();
                modelEntity.Full = model.Full;
                modelEntity.Title = model.Title;
                modelEntity.Short = model.Short;
                modelEntity.AllowComments = true;
                modelEntity.Published = true;
                modelEntity.Id = 0;
                modelEntity.LanguageId = 1;
                modelEntity.MetaKeywords = model.MetaKey;
                modelEntity.MetaDescription = model.MetaDescription;
                modelEntity.MetaTitle = model.MetaTitle;
                var newsItem = modelEntity.ToEntity<NewsItem>();
                newsItem.CreatedOnUtc = DateTime.UtcNow;
                _newsService.InsertNews(newsItem);
                //activity log
                _customerActivityService.InsertActivity("AddNewNews",string.Format(_localizationService.GetResource("ActivityLog.AddNewNews"), newsItem.Id), newsItem);
                    //search engine name
                var seName = _urlRecordService.ValidateSeName(newsItem, modelEntity.SeName, model.Title, true);
                _urlRecordService.SaveSlug(newsItem, seName, newsItem.LanguageId);

                //Stores
                SaveStoreMappings(newsItem, modelEntity);
                _productService.UpdateUpdatedDate();

                return new JsonResult("OK");
            }
         return new JsonResult("Data Invalid");
        }


        protected virtual void SaveStoreMappings(NewsItem newsItem, NewsItemModel model)
        {
            newsItem.LimitedToStores = model.SelectedStoreIds.Any();
            _newsService.UpdateNews(newsItem);

            var existingStoreMappings = _storeMappingService.GetStoreMappings(newsItem);
            var allStores = _storeService.GetAllStores();
            foreach (var store in allStores)
            {
                if (model.SelectedStoreIds.Contains(store.Id))
                {
                    //new store
                    if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
                        _storeMappingService.InsertStoreMapping(newsItem, store.Id);
                }
                else
                {
                    //remove store
                    var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                    if (storeMappingToDelete != null)
                        _storeMappingService.DeleteStoreMapping(storeMappingToDelete);
                }
            }
        }
      
    }
}