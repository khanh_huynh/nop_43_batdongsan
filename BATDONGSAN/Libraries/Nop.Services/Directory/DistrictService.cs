﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Data;
using Nop.Services.Caching;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Directory
{
    /// <summary>
    /// State province service
    /// </summary>
    public partial class DistrictService : IDistrictService
    {
        #region Fields

        private readonly ICacheKeyService _cacheKeyService;
         private readonly IStaticCacheManager _staticCacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly IRepository<District> _districtRepository;

        #endregion

        #region Ctor

        public DistrictService(ICacheKeyService cacheKeyService,
            IStaticCacheManager staticCacheManager,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<District> districtRepository)
        {
            _cacheKeyService = cacheKeyService;
            _staticCacheManager = staticCacheManager;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _districtRepository = districtRepository;
        }

        #endregion

      
        public virtual District GetDistrictById(int districtId)
        {
            if (districtId == 0)
                return null;

            return _districtRepository.ToCachedGetById(districtId);
        }

        public virtual IList<District> GetDistrictByProvinceId( int? provinceId = null)
        {

            var key = _cacheKeyService.PrepareKeyForDefaultCache(NopDirectoryDefaults.DistrictByProvinceIdCacheKey,provinceId ?? 0);

            var query = _districtRepository.Table.AsQueryable();

            //filter by country
            if (provinceId.HasValue)
                query = query.Where(state => state.ProvinceId == provinceId);
                
            return query.ToCachedArray(key);
        }

    }
}