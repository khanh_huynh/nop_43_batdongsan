﻿using System.Collections.Generic;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;

namespace Nop.Services.Directory
{
    /// <summary>
    /// State province service interface
    /// </summary>
    public partial interface IDistrictService
    {
        District GetDistrictById(int DistrictId);

        IList<District> GetDistrictByProvinceId( int? provinceId = null);

    }
}
