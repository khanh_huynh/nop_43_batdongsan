import scrapy
from scrapy.crawler import CrawlerProcess
from spider_list import SpiderList
from spider_details import SpiderDetails

process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
})

# process.crawl(SpiderList)
# process.start() # the script will block here until the crawling is finished
process.crawl(SpiderDetails)
try:
    process.start() 
except Exception as ex:
    print(ex)
