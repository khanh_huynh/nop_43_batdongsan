import scrapy
import requests
import hashlib
import datetime
import json
import time
from datetime import date   

class SpiderDetails(scrapy.Spider):
    hdl = open("./test/list.txt", 'r')
    milist = hdl.readlines()
    hdl.close()
    name = 'cafeland'
    start_urls = milist
    arrTemp = []
    for page in start_urls:
        page = page.replace("\n","")
        arrTemp.append(page)

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse, headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36"})
            time.sleep(5)

    def parse(self, response):
        #for content in response.css('#sevenBoxNewContentInfo'):
        print("OK-----------------------------------------------------")
        content =  str(response.css('#sevenBoxNewContentInfo').extract())
        title = response.css('.sevenPostTitle a::text').get()
        header = response.css(".sevenPostDes::text").get()
        content= content.replace("CafeLand","DuchungLand")
        if header != None:
            header = header.replace("CafeLand","DuchungLand")
        contenRep = content.replace("['","").replace("']","").replace("src","srr").replace("data-srr","src").replace("z-index: 1000;","").replace("height:","heightt:").replace("width:","widthh:")
        copy_right = '<div style="text-align:right;color: gainsboro;">Nguồn '+ response.url+'</div>'
        contenRep =contenRep + copy_right
        head_image = response.xpath("//img/@src").extract()
        images = []
        today = date.today().isoformat()
        toadyString = today.replace("-","/")
        
        for h in head_image:
            if "/cafelandnew/hinh-anh/"+toadyString in h:
                images.append(h)
        scraped_info = {
                'MetaKey': response.url,
                'Title' : title,
                'Full' : contenRep,
                'Short': header,
                'MetaTitle':images[0]
            }
        today = datetime.date.today()
       
        todayFormat = today.strftime('%Y%m%d')
        print(todayFormat)
        url = 'http://duchungland.vn/Admin/NewsApi/Add'
        stringFormat = "NP430_BDS"+todayFormat
        print(stringFormat)
        auth = hashlib.md5(stringFormat.encode('utf-8')).hexdigest()

        headers={
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "authorization":auth
            }

        d = json.dumps(scraped_info)

        response = requests.post(url, data=d,headers=headers)
        print('RESPONSE------------------------------------------------'+response.json())



          

